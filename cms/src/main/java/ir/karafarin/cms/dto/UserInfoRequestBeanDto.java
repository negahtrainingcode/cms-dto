package ir.karafarin.cms.dto;

import com.sun.istack.internal.NotNull;

public class UserInfoRequestBeanDto {
    Boolean includeSubscribers;
    @NotNull
    String password;
    @NotNull
    String username;
}
