package ir.karafarin.cms.dto;

import com.sun.istack.internal.NotNull;
import lombok.Data;

@Data
public class CardSummaryRequestBeanDto {
    @NotNull
    String cardPan;
    Boolean showIban;
}
