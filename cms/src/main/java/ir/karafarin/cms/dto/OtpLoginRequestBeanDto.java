package ir.karafarin.cms.dto;

import com.sun.istack.internal.NotNull;

public class OtpLoginRequestBeanDto {
    enum AuthenticationTypeWS {STATIC_USERNAME_CHANNEL, OTP1, OTP2, OTP3};
    @NotNull
    AuthenticationTypeWS authenticationType;
    String challengeIdentifier;
    String otpSync;
//    Boolean includeSubscribers;
//    @NotNull
//    String password;
//    @NotNull
//    String username;
}
