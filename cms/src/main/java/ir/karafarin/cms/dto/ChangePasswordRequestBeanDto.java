package ir.karafarin.cms.dto;

import com.sun.istack.internal.NotNull;

public class ChangePasswordRequestBeanDto {
    @NotNull
    String currentPassword;
    @NotNull
    String newPassword;
}
