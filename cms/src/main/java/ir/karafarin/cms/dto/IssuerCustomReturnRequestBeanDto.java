package ir.karafarin.cms.dto;

import com.sun.istack.internal.NotNull;
import lombok.Data;
import org.springframework.lang.Nullable;

import java.math.BigDecimal;

@Data
public class IssuerCustomReturnRequestBeanDto {
    enum ClientType {ATM, POS, PINPAD, INTERNET, VRU, POSCONDCOD, PHONE, OTHER};
    @NotNull
    BigDecimal amount;
    @NotNull
    ClientType clientType;
    @NotNull
    String merchantId;
    @NotNull
    String referenceNumber;
    @NotNull
    String resNum;
    @NotNull
    String terminalId;
}
