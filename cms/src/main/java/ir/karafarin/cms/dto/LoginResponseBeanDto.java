package ir.karafarin.cms.dto;

import com.sun.xml.internal.ws.api.ha.StickyFeature;

import java.util.List;

public class LoginResponseBeanDto {
    enum GenderWS {MALE, FEMALE, LEGAL, PUBLIC};
    String cif;
    String code;
    String email;
    GenderWS gender;
    DateTime lastLoginTime;
    String mobile;
    String name;
    String refreshToken;
    String sessionId;
    List<SubscriberUserInfoBeanDto> subscribers;
    String temporarySessionId;
    String title;
}
