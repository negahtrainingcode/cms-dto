package ir.karafarin.cms.dto;

import lombok.Data;

@Data
public class SubscriberUserInfoBeanDto {
    String id;
    String name;
}
