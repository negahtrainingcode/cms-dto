package ir.karafarin.cms.dto;

import lombok.Data;

@Data
public class CardsSearchRequestBeanDto {
    enum CardStatus {OK, HOT, WARM, BLOCKED, CAPTURED, EXPIRED, INACTIVE, SETTLEMENT, CLOSED};
    CardStatus cardStatus;
    String cif;
    String depositNumber;
    long length;
    long offset;
    String pan;

}
