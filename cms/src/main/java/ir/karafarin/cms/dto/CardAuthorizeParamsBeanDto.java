package ir.karafarin.cms.dto;

import com.sun.istack.internal.NotNull;

import lombok.Data;

@Data
public class CardAuthorizeParamsBeanDto {
    enum PinType {CARD, EPAY};
    String cvv2;
    String expDate;
    @NotNull
    String pin;
    @NotNull
    PinType pinType;
    String track2;
}
