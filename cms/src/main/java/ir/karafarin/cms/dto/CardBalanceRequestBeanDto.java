package ir.karafarin.cms.dto;

import com.sun.istack.internal.NotNull;
import lombok.Data;

@Data
public class CardBalanceRequestBeanDto {
    @NotNull
    CardAuthorizeParamsBeanDto cardAuthorizeParams;
    String depositNumber;
    String merchantId;
    @NotNull
    String pan;
}
