package ir.karafarin.cms.dto;

import com.sun.istack.internal.NotNull;
import com.sun.xml.internal.ws.api.ha.StickyFeature;

import java.math.BigDecimal;

public class FundTransferRequestBeanDto {
    enum DepositOrPanWS {PAN, DEPOSIT};
    enum PinType {CARD, EPAY};
    @NotNull
    BigDecimal amount;
    String cvv2;
    @NotNull
    String destination;
    @NotNull
    DepositOrPanWS destinationType;
    String expDate;
    String merchantId;
    @NotNull
    String pan;
    @NotNull
    String pin;
    @NotNull
    PinType pinType;
    String pspCode;
    String track2;
    String twoPhaseReferenceCode;
}
