package ir.karafarin.cms;

import ir.karafarin.cms.dto.*;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

public class RestAPIs {

    public void consume() {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_XML);

        String tosanUrl = "{context name}/user/loginStatic";
        HttpEntity<UserInfoRequestBeanDto> loginStaticRequest = new HttpEntity<>(new UserInfoRequestBeanDto(), headers);
        Object loginStaticResponse = restTemplate.postForObject(tosanUrl, loginStaticRequest, Object.class);

        tosanUrl = "{context name}/user/logout";
        HttpEntity<IssuerCustomReturnRequestBeanDto> logoutRequest = new HttpEntity<>(new IssuerCustomReturnRequestBeanDto(), headers);
        Object logoutResponse = restTemplate.postForObject(tosanUrl, logoutRequest, Object.class);

        tosanUrl = "{context name}/setting/changePassword";
        HttpEntity<ChangePasswordRequestBeanDto> changePasswordRequest = new HttpEntity<>(new ChangePasswordRequestBeanDto(), headers);
        Object changePasswordResponse = restTemplate.postForObject(tosanUrl, changePasswordRequest, Object.class);

        tosanUrl = "{context name}/card";
        HttpEntity<CardsSearchRequestBeanDto> cardRequest = new HttpEntity<>(new CardsSearchRequestBeanDto(), headers);
        Object cardResponse = restTemplate.postForObject(tosanUrl, cardRequest, Object.class);

        tosanUrl = "{context name}/user/getCardSummary";
        HttpEntity<CardSummaryRequestBeanDto> getCardSummaryRequest = new HttpEntity<>(new CardSummaryRequestBeanDto(), headers);
        Object getCardSummaryResponse = restTemplate.postForObject(tosanUrl, getCardSummaryRequest, Object.class);

        tosanUrl = "{context name}/card/balanceInquery";
        HttpEntity<CardBalanceRequestBeanDto> balanceInqueryRequest = new HttpEntity<>(new CardBalanceRequestBeanDto(), headers);
        Object balanceInqueryResponse = restTemplate.postForObject(tosanUrl, balanceInqueryRequest, Object.class);

        tosanUrl = "{context name}/card/transfer";
        HttpEntity<FundTransferRequestBeanDto> transferRequest = new HttpEntity<>(new FundTransferRequestBeanDto(), headers);
        Object transferResponse = restTemplate.postForObject(tosanUrl, transferRequest, Object.class);


    }
}
